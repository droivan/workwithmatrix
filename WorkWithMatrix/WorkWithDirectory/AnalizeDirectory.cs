﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using WorkWithMatrix.ReadFromFile;

namespace WorkWithMatrix.WorkWithDirectory
{
    public class AnalizeDirectory
    {
        private string dirName;

        public string DirName { get => dirName; set => dirName = value; }

        /**
         * Проверяем существуетли каталог и файлы в нем
         */
        public int ViewDir()
        {
            int dirOrFileExist = 1;
            if (Directory.Exists(DirName))
            {
                Console.WriteLine("Текущий каталог:\n\t{0}", DirName);
                string[] files = Directory.GetFiles(DirName);

                if (files.Length == 0)
                    dirOrFileExist = -2;
                else
                {
                    MatrixProcess newRead = new MatrixProcess();

                    foreach (string fileName in files)
                    {
                        newRead.PathToFile = fileName;
                        if (newRead.ReadFile() != 0)
                            continue;
                    }
                }

            }
            else
            {
                dirOrFileExist = -1;
            }
            return dirOrFileExist;
        }
    }
}
