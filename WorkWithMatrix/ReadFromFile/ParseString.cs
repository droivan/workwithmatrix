﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithMatrix.ReadFromFile
{
    public static class ParseString
    {
        public static List<int> FillIntListFromString(ref string str)
        {
            /***
             * Формат строки "21 33 4 46 551"
             * любое отклонение - ошибка формата
             */
            string value = "";
            List<int> arrList = new List<int>();

            foreach (char c in str)
            {
                if ((c == '+' && value == string.Empty) || (c == '-' && value == string.Empty) || char.IsDigit(c))
                {
                    value += c;
                }
                else if (c == ' ' && !string.IsNullOrEmpty(value) && char.IsDigit(value[value.Length - 1]))
                {
                    arrList.Add(Convert.ToInt32(value));
                    value = "";
                }
                else
                    throw new ArgumentException("Некорректные исходные данные в строке матрицы");
            }
            if (!string.IsNullOrEmpty(value))
                arrList.Add(Convert.ToInt32(value));

            return arrList;
        }
    }
}
