﻿using System;
using System.Collections.Generic;

using System.IO;
using WorkWithMatrix.Math;
using WorkWithMatrix.WriteToFile;

namespace WorkWithMatrix.ReadFromFile
{
    public class MatrixProcess
    {
        private string pathToFile = "";

        private List<List<int>> matrixA = null;
        private List<List<int>> matrixB = null;
        private List<List<int>> resultMatrix = null;

        public string PathToFile { get => pathToFile; set => pathToFile = value; }
        
        /**
         * Чтение файла, проверка структуры файла, расчет итоговой матрицы, сохранение 
         */
        public int ReadFile()
        {
            int hasError = 0;
            if (string.IsNullOrEmpty(pathToFile))
                return -1;

            try
            {
                Console.WriteLine("\nФайл: {0}", PathToFile.Substring(PathToFile.LastIndexOf('\\') + 1));
                using (StreamReader sr = new StreamReader(PathToFile, System.Text.Encoding.Default))
                {
                    /***
                     * Анализируем первую строку
                     * Для операций + - * используются только 2 матрицы
                     * Для операции транспонирования требуется минимум 1 матрица
                     */
                    string operation;
                    byte mode = 0;

                    if ((operation = sr.ReadLine()) != null)
                    {
                        sr.Close();
                        
                        switch (operation.ToLower())
                        {
                            case "multiply":
                            {
                                mode = 1;
                                break;
                            }
                            case "add":
                            {
                                mode = 2;
                                break;
                            }
                            case "subtract":
                            {
                                mode = 3;
                                
                                break;
                            }
                            case "transpose":
                            {
                                mode = 4;
                                break;
                            }
                            default:
                            {
                                throw new ArgumentException("Ошибка формата файла. Недопустимая операция с матрицами");
                            }
                        }

                        Console.WriteLine("\nОперация {0}", operation);
                        CreateResultMatrix(mode);
                    }                    
                }

                Console.WriteLine();
            }
            catch (Exception e)
            {
                /***
                 * Ловим все исключения, записываем их в файл и
                 * завершаем работу с текущим файлом исходных данных
                 */
                SaveResultToFile.SaveBadResult(PathToFile.Substring(0, PathToFile.IndexOf(".txt")), e.Message);
                Console.WriteLine(e.Message);
                hasError = -1;
            }
            if (hasError == 0)
                Console.WriteLine("Работа с файлом завершена успешно!");

            return hasError;
        }

        /**
         * mode - тип операции:
         *  1 - *
         *  2 - +
         *  3 - -
         *  4 - Т
         * PS: функция ужасна. Изначально хотел при транспонировании читать 1 матрицу,
         * запоминать позицию в файле и в другом методе/классе транспонировать ее,
         * затем возвратитья обратно и читать с сохраненной позиции новую матрицу.
         * StreamReader не отображает текущую позицию, т.к. ReadLine читает в буфер данные.
         * Затем хотел читать побайтно через FileStream, формируя строку. С ходу не получилось.
         * В общем, за эту функцию мне стыдно. 
         */
        public void CreateResultMatrix(byte mode) 
        {
            using (StreamReader sr = new StreamReader(PathToFile, System.Text.Encoding.Default))
            {
                string line;

                //Пропускаем первую строку с опредением операции
                line = sr.ReadLine();

                //Если следующая строка пустая - все ок согласно структуре файла
                if ((line = sr.ReadLine()) != null && string.IsNullOrEmpty(line.Replace(" ", "")))
                {
                    //Инициализация матрицы А
                    matrixA = new List<List<int>>();
                    if (mode != 4)
                        //Инициализация матрицы B
                        matrixB = new List<List<int>>();
                    /**
                     * Полностью читаем файл
                     */
                    bool filledMatrixA = false; // Матрица А заполнена
                    List<int> tmpList;
                    while ((line = sr.ReadLine()) != null)
                    {
                        line = line.Trim();
                        if (line == string.Empty)
                        {
                            if ( matrixA.Count != 0)
                                if (mode == 4)
                                {
                                    resultMatrix = MathOperations.Transpose(ref matrixA);
                                    SaveResultToFile.SaveResult(PathToFile.Substring(0, PathToFile.IndexOf(".txt")),
                                                                ref resultMatrix, true);
                                    PrintResultMatrix();
                                    Console.WriteLine('\n');
                                    filledMatrixA = false;

                                    foreach (List<int> list in matrixA)
                                    {
                                        list.Clear();
                                    }
                                    matrixA.Clear();
                                }
                                else
                                {
                                    // Прочитали первую матрицу, читаем вторую
                                    filledMatrixA = true;
                                    Console.WriteLine();
                                }

                            //Пропускаем пустую строку, переходим к следующей матрице
                            line = sr.ReadLine();
                        }
                        if (!filledMatrixA && (line != string.Empty))
                        {
                            tmpList = (ParseString.FillIntListFromString(ref line));
                            if (tmpList.Count != 0)
                                matrixA.Add(new List<int>(tmpList));
                            else
                            {
                                throw new ArgumentException("Некорректные исходные данные матрицы А. Строка в файле не содержит цифр");
                            }
                        }
                        else if (mode != 4)
                        {
                            // Заполнили первую матрицу, заполняем вторую
                            tmpList = (ParseString.FillIntListFromString(ref line));
                            if (tmpList.Count != 0)
                                matrixB.Add(new List<int>(tmpList));
                            else
                            {
                                throw new ArgumentException("Некорректные исходные данные матрицы В. Строка в файле не содержит цифр");
                            }

                        }
                        Console.WriteLine(line);
                    }
                    
                    /**
                     * Транспонирование выполняется и в цикле после формирования матрицы, т.к
                     * матриц может быть больше одной                     *
                     */
                    switch (mode)
                    {
                        case 1:
                        {
                            resultMatrix = MathOperations.Multiply(ref matrixA, ref matrixB);
                            break;
                        }

                        case 2:
                        {
                            resultMatrix = MathOperations.Add(ref matrixA, ref matrixB);
                            break;
                        }
                        case 3:
                        {
                            resultMatrix = MathOperations.Subtract(ref matrixA, ref matrixB);
                            break;
                        }
                        case 4:
                        {
                            resultMatrix = MathOperations.Transpose(ref matrixA);
                            break;
                        }
                    }

                    SaveResultToFile.SaveResult(PathToFile.Substring(0, PathToFile.IndexOf(".txt")), 
                                                ref resultMatrix,
                                                mode == 4);
                    PrintResultMatrix();
                }
                else
                {
                    // 
                    throw new ArgumentException("Ошибка формата файла. Пустая строка не обнаружена");
                }
            }
        }

        /**
         * Вывод результирующей матрицы на экран
         */
        private void PrintResultMatrix()
        {
            if (resultMatrix != null && resultMatrix.Count > 0)
            {
                Console.WriteLine("\nРезультирующая матрица:");
                for (int i = 0; i < resultMatrix.Count; i++)
                {
                    for (int j = 0; j < resultMatrix[i].Count; j++)
                    {
                        Console.Write("{0:D000} ",resultMatrix[i][j]);
                    }

                    Console.WriteLine();
                }
            }
            else
                Console.WriteLine("Результирующая матрица пуста!");
        }
    }
}
