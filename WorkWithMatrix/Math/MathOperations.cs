﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithMatrix.Math
{
    public class MathOperations
    {
        /***
         * 2 матрицы можно перемножать между собой тогда и только тогда,
         * когда количество столбцов 1 матрицы равно количеству строк второй
         * Am*n x Bn*k = Cm*k
         * cij = ai1*b1j + ai2*b2j + ... + ain*bnj
         */
        public static List<List<int>> Multiply(ref List<List<int>> matrixA, ref List<List<int>> matrixB)
        {
            List<List<int>> resultMatrix = new List<List<int>>();

            int rowsCountA = matrixA.Count;
            int columnsCountA = matrixA[0].Count;
            int rowsCountB = matrixB.Count;
            int columnsCountB = matrixB[0].Count;
            
            if (rowsCountA != 0 && columnsCountA != 0 && rowsCountB != 0 && columnsCountB != 0)
            {
                if (columnsCountA == rowsCountB)
                {
                    // Можно перемножать
                    for (int i = 0; i < rowsCountA; i++)
                    {
                        resultMatrix.Add(new List<int>());
                        for (int j = 0; j < columnsCountB; j++)
                        {
                            resultMatrix[i].Add(0);
                            for (int k = 0; k < columnsCountA; k++)
                                resultMatrix[i][j] += matrixA[i][k] * matrixB[k][j];
                        }
                    }
                }
                else
                {
                    throw new ArgumentException("Матрицы не подходят для перемножения. Матрицы не согласованы");
                }
            }
            else
            {
                throw new ArgumentException("Операция умножения матриц. Матрица(ы) пуста(ы)!");
            }
            return resultMatrix;
        }

        /***
         * Складывать можно матрицы только одного размера
         * cij = aij + bij
         */
        public static List<List<int>> Add(ref List<List<int>> matrixA, ref List<List<int>> matrixB)
        {
            List<List<int>> resultMatrix = new List<List<int>>();
            int rowsCountA = matrixA.Count;
            int columnsCountA = matrixA[0].Count;
            int rowsCountB = matrixB.Count;
            int columnsCountB = matrixB[0].Count;

            if (rowsCountA != 0 && columnsCountA != 0 && rowsCountB != 0 && columnsCountB != 0)
            {
                if (rowsCountA == rowsCountB && columnsCountA == columnsCountB)
                {
                    // Матрицы одного размера
                    for (int i = 0; i < rowsCountA; i++)
                    {
                        resultMatrix.Add(new List<int>());
                        for (int j = 0; j < columnsCountB; j++)
                        {
                            resultMatrix[i].Add(matrixA[i][j] + matrixB[i][j]);
                        }
                    }
                }
                else
                {
                    throw new ArgumentException("Матрицы не подходят для сложения. Матрицы не согласованы");
                }
            }
            else
            {
                throw new ArgumentException("Операция сложения матриц. Матрица(ы) пуста(ы)!");
            }

            return resultMatrix;
        }

        /***
         * Вычитать можно матрицы только одного размера
         * cij = aij - bij
         */
        public static List<List<int>> Subtract(ref List<List<int>> matrixA, ref List<List<int>> matrixB)
        {
            List<List<int>> resultMatrix = new List<List<int>>();
            int rowsCountA = matrixA.Count;
            int columnsCountA = matrixA[0].Count;
            int rowsCountB = matrixB.Count;
            int columnsCountB = matrixB[0].Count;

            if (rowsCountA != 0 && columnsCountA != 0 && rowsCountB != 0 && columnsCountB != 0)
            {
                if (rowsCountA == rowsCountB && columnsCountA == columnsCountB)
                {
                    // Матрицы одного размера
                    for (int i = 0; i < rowsCountA; i++)
                    {
                        resultMatrix.Add(new List<int>());
                        for (int j = 0; j < columnsCountB; j++)
                        {
                            resultMatrix[i].Add(matrixA[i][j] - matrixB[i][j]);
                        }
                    }
                }
                else
                {
                    throw new ArgumentException("Матрицы не подходят для вычитания. Матрицы не согласованы");
                }
            }
            else
            {
                throw new ArgumentException("Операция вычитания матриц. Матрица(ы) пуста(ы)!");
            }

            return resultMatrix;
        }

        /***
         * Транспонирование - операция, в которой строки и столбцы меняются местами
         * Aij = Aji
         */
        public static List<List<int>> Transpose(ref List<List<int>> matrixA)
        {
            List<List<int>> resultMatrix = new List<List<int>>();

            int rowsCountA = matrixA.Count;
            int columnsCountA = matrixA[0].Count;

            if (rowsCountA != 0 && columnsCountA != 0)
            {
                for (int j = 0; j < columnsCountA; j++)
                {
                    resultMatrix.Add(new List<int>(columnsCountA));// Добавили строки
                    for (int i = 0; i < rowsCountA; i++)
                    {
                        resultMatrix[j].Add((matrixA[i][j]));
                    }
                }
            }
            else
            {
                throw new ArgumentException("Операция транспонирования матрицы. Матрица пуста!");
            }

            return resultMatrix;
        }
    }
}
