﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithMatrix.WriteToFile
{
    public class SaveResultToFile
    {
        private static string resultSuffix = @"_result.txt";
        public static string ResultSuffix { get => resultSuffix; }

        public static void SaveBadResult(string path, string message)
        {
            using (StreamWriter sw = new StreamWriter(path + ResultSuffix))
            {
                sw.WriteLine(message);
            }
        }

        /**
         * appendMode:
         *  false - записать в новый файл
         *  true  - добавить в конец файла (при транспонировании)
         *
         */
        public static void SaveResult(string path, ref List<List<int>> resultMatrix, bool appendMode)
        {
            using (StreamWriter sw = new StreamWriter(path + ResultSuffix, appendMode))
            {
                if (resultMatrix != null && resultMatrix.Count > 0)
                {
                    for (int i = 0; i < resultMatrix.Count; i++)
                    {
                        for (int j = 0; j < resultMatrix[i].Count; j++)
                        {
                            sw.Write("{0:D000} ", resultMatrix[i][j]);
                        }
                        sw.WriteLine();
                    }
                    sw.WriteLine();
                }
                else
                    Console.WriteLine("Результирующая матрица пуста!");
            }
        }
    }
}
