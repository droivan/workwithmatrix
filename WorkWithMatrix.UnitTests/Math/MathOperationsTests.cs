﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WorkWithMatrix.Math;

namespace WorkWithMatrix.UnitTests.Math
{
    [TestFixture]
    public class MathOperationsTests
    {
        [Test]
        public void Multiply_CheckDimentionsTryMultiply_RigthResult()
        {
            List<List<int>> matrixA = FillMatrix(2, 4, 1);
            List<List<int>> matrixB = FillMatrix(4, 3, 2);
            List<List<int>> expectedResult = new List<List<int>>();
            expectedResult.Add(new List<int>() { 28, 40, 52 });
            expectedResult.Add(new List<int>() { 40, 60, 80 });

            List<List<int>> resultC = MathOperations.Multiply(ref matrixA, ref matrixB);

            Assert.AreEqual(expectedResult, resultC);

        }

        [Test]
        public void Add_CheckDimentionsTryAdd_RigthResult()
        {
            List<List<int>> matrixA = FillMatrix(2, 4, 1);
            List<List<int>> matrixB = FillMatrix(2, 4, 2);
            List<List<int>> expectedResult = new List<List<int>>();
            expectedResult.Add(new List<int>() { 0, 3, 6, 9 });
            expectedResult.Add(new List<int>() { 3, 6, 9, 12 });

            List<List<int>> resultC = MathOperations.Add(ref matrixA, ref matrixB);

            Assert.AreEqual(expectedResult, resultC);
        }

        [Test]
        public void Subtract_CheckDimentionsTrySubtract_RigthResult()
        {
            List<List<int>> matrixA = FillMatrix(2, 4, 1);
            List<List<int>> matrixB = FillMatrix(2, 4, 2);
            List<List<int>> expectedResult = new List<List<int>>();
            expectedResult.Add(new List<int>() { 0, -1, -2, -3 });
            expectedResult.Add(new List<int>() { -1, -2, -3, -4 });

            List<List<int>> resultC = MathOperations.Subtract(ref matrixA, ref matrixB);

            Assert.AreEqual(expectedResult, resultC);
        }

        [Test]
        public void Transpose_CheckDimentionsTryTranspose_RigthResult()
        {
            List<List<int>> matrixA = FillMatrix(2, 4, 1);
            List<List<int>> expectedResult = new List<List<int>>
            {
                new List<int>() { 0, 1 },
                new List<int>() { 1, 2 },
                new List<int>() { 2, 3 },
                new List<int>() { 3, 4 }
            };
            List<List<int>> resultC = MathOperations.Transpose(ref matrixA);

            Assert.AreEqual(expectedResult, resultC);
        }

        private List<List<int>> FillMatrix(int m, int n, int coeff)
        {
            List<List<int>> mas = new List<List<int>>();    //динамический двумерный массив
            for (int i = 0; i < m; i++)
            {
                var row = new List<int>();
                for (int j = 0; j < n; j++)
                    row.Add((i + j)* coeff);                //строка массива заполняется просто суммой i и j
                mas.Add(row);                               //строка добавляется в массив
            }

            return mas;
        }
    }
}
