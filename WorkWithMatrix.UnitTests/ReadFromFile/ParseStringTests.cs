﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;
using WorkWithMatrix.ReadFromFile;

namespace WorkWithMatrix.UnitTests.ReadFromFile
{
    [TestFixture]
    class ParseStringTests
    {
        [Test]
        public void FillIntListFromString_CheckFilingArray_RigthFill()
        {

            List<int>[] resultList =
            {
                new List<int> {12, 44, -23, 0, 111},
                new List<int> {1, 2, 0, 34},
                new List<int> {14}
            };

            string [] dataStr =
            {
                " 12 44 -23 0 111 ",
                " +1 2 0 34   ",
                "    +14    "
            };

            for (int i = 0; i < resultList.Length; i++)
            {
                /***
                 * В метод FillIntListFromString заранее передается строка
                 * после выполнения string.Trim()
                 */
                dataStr[i] = dataStr[i].Trim();
                Assert.AreEqual(ParseString.FillIntListFromString(ref dataStr[i]), resultList[i]);
            }
        }

        [TestCase("    12   -44 +23 +0 111 ")]
        [TestCase(" 12   -44 +23 +0 111+ ")]
        [TestCase(" 12 --44 +23 +0 111 ")]
        [TestCase(" 12 --44 +23 +0 1,11 ")]
        [TestCase(" 12.0 --44 +23 +0 1,11 ")]
        [TestCase(" ++12 ")]
        [TestCase(" 2.1 ")]
        [TestCase(" 2,1 ")]
        [TestCase(" 1d")]
        [TestCase(" 1-883 ")]
        [TestCase(" - 3 ")]
        public void FillIntListFromString_EnterWrongString_GetException(string dataStr)
        {
            dataStr = dataStr.Trim();
            var ex = Assert.Catch<Exception>(() => ParseString.FillIntListFromString(ref dataStr));
            StringAssert.Contains("Некорректные исходные данные в строке матрицы", ex.Message);
        }
    }
}
