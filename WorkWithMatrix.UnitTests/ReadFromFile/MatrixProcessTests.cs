﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WorkWithMatrix;
using WorkWithMatrix.ReadFromFile;

namespace WorkWithMatrix.UnitTests.ReadFromFile
{
    [TestFixture]
    class MatrixProcessTests
    {
        // myPathFolder - папка сисходными данными
        private string myPathFolder = @"D:\My_code\C#\WorkWithMatrix\TestFiles";
        [Test]
        public void ReadFile_OpenFileToMultiply_OpenWithErrors()
        {
            MatrixProcess newRead = new MatrixProcess();
            newRead.PathToFile = myPathFolder+"\\multiply1.txt";

            Assert.AreEqual(newRead.ReadFile(), -1);
        }

        [Test]
        public void ReadFile_OpenFileToTranspose_OpenWithoutErrors()
        {
            MatrixProcess newRead = new MatrixProcess();
            newRead.PathToFile = myPathFolder + "\\transpose1.txt";

            Assert.AreEqual(newRead.ReadFile(), 0);
        }

        [Test]
        public void ReadFile_OpenFileToTransposeWith2matrix_OpenWithoutErrors()
        {
            MatrixProcess newRead = new MatrixProcess();
            newRead.PathToFile = myPathFolder + "\\transpose2.txt";

            Assert.AreEqual(newRead.ReadFile(), 0);
        }

        [Test]
        public void ReadFile_OpenFileToAdd_OpenWithoutErrors()
        {
            MatrixProcess newRead = new MatrixProcess();
            newRead.PathToFile = myPathFolder + "\\Add1.txt";

            Assert.AreEqual(newRead.ReadFile(), 0);
        }

        [Test]
        public void ReadFile_OpenFileToSubtract_OpenWithoutErrors()
        {
            MatrixProcess newRead = new MatrixProcess();
            newRead.PathToFile = myPathFolder + "\\subtract1.txt";

            Assert.AreEqual(newRead.ReadFile(), 0);
        }

    }
}
